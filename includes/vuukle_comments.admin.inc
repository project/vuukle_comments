<?php

/**
 * @file
 * Admin settings form and callbacks.
 */

/**
 * Implements hook_settings_form().
 */
function vuukle_comments_settings_form() {
  $node_type = node_type_get_names();
  foreach ($node_type as $key => $value) {
    $nodetype[$key] = ucwords($value);
  }
  $form['vuukle_comments_use_config'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable Vuukle Configuration'),
    '#description' => t('Check this checkbox will enable vuukle configuration.'),
    '#options' => drupal_map_assoc(array(
      t('yes'),
    )
    ),
    '#default_value' => variable_get('vuukle_comments_use_config', array()),
  );
  $form['vuukle_comments_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select content type for which you want vuukle enable'),
    '#options' => $nodetype,
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_content_type', array()),
  );
  $form['vuukle_comments_rating_text'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Rating text'),
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_rating_text'),
  );
  $form['vuukle_comments_text_0'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Comment text zero'),
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_text_0'),
  );
  $form['vuukle_comments_text_1'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Comment text one'),
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_text_1'),
  );
  $form['vuukle_comments_text_multi'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Comment Text Multi'),
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_text_multi'),
  );
  $form['vuukle_comments_ga_code'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Google Analytics code'),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_ga_code'),
  );
  $form['vuukle_comments_api_key'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Vuukle api key'),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_api_key'),
  );
  $form['vuukle_comments_col_code'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Vuukle Col Code'),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_col_code'),
  );
  $form['vuukle_comments_stories_title'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#title' => t('Vuukle Stories title'),
    '#states' => array(
      'visible' => array(
        ':input[name="vuukle_comments_use_config[yes]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('vuukle_comments_stories_title'),
  );
  return system_settings_form($form);
}
