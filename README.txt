
-- SUMMARY --

This module provide the vuukle integration with drupal 7 nodes.
Make sure to use vuukle integration for commenting disable node commenting
configuration.

-- REQUIREMENTS --
  No

-- INSTALLATION --
* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

-- CUSTOMIZATION --
* Access menu from here : admin/config/vuukle-config/settings

-- CONTACT --
Current maintainers:
* Ankush Gautam https://www.drupal.org/u/agautam
  email : ankushgautam76@gmail.com
