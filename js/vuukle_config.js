/**
 * @file
 * Attaches behaviors for the Vuukle comments module.
 */
(function ($) {

  "use strict";

  /**
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.vuukle_comments = {
    attach: function (context, settings) {
      var VUUKLE_CUSTOM_TEXT = '{ "rating_text": '+ settings.vuukle_comments.settings.vuukle_comments_rating_text +', "comment_text_0": '+ settings.vuukle_comments.settings.vuukle_comments_text_0 +', "comment_text_1": '+ settings.vuukle_comments.settings.vuukle_comments_text_1 +', "comment_text_multi": '+ settings.vuukle_comments.settings.vuukle_comments_text_multi +', "stories_title": '+ settings.vuukle_comments.settings.vuukle_comments_stories_title +' }';
      var UNIQUE_ARTICLE_ID = settings.vuukle_comments.settings.nodeid;
      var SECTION_TAGS = "";// TO DO
      var ARTICLE_TITLE = settings.vuukle_comments.settings.node_title;
      var GA_CODE = settings.vuukle_comments.settings.vuukle_comments_ga_code;
      var VUUKLE_API_KEY = settings.vuukle_comments.settings.vuukle_comments_api_key;
      var TRANSLITERATE_LANGUAGE_CODE = 'en';//TO DO Node language
      var VUUKLE_COL_CODE = settings.vuukle_comments.settings.vuukle_comments_col_code;
      var ARTICLE_AUTHORS = {};
      create_vuukle_platform(VUUKLE_API_KEY, UNIQUE_ARTICLE_ID, "0", SECTION_TAGS, ARTICLE_TITLE, TRANSLITERATE_LANGUAGE_CODE, "1", "", GA_CODE, VUUKLE_COL_CODE, ARTICLE_AUTHORS);
    }
  };

})(jQuery, Drupal, this, this.document);
